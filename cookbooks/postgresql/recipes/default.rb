package 'postgresql'

shmmax = 1024 * 1024 * 1024 * 1024 * 20 # 20 GB

file '/etc/sysctl.d/30-postgresql-shm.conf' do
  content "kernel.shmmax = #{shmmax}\n"
  notifies :run, 'execute[set-shmmax]'
end

execute 'set-shmmax' do
  command "sysctl -w kernel.shmmax=#{shmmax}"
  action :nothing
end

template '/etc/postgresql/9.1/main/postgresql.conf' do
  user 'postgres'
  group 'postgres'
  mode 0644
  notifies :restart, 'service[postgresql]'
end

service 'postgresql' do
  action [:enable, :start]
  supports :restart => true
end
