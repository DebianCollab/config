backend default {
    .host = "127.0.0.1";
    .port = "8080";
}
include "/etc/noosfero/varnish-noosfero.vcl";
include "/etc/noosfero/varnish-accept-language.vcl";
