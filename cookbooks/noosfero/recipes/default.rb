directory "#{Chef::Config[:file_cache_path]}" do
  action :create
  recursive true
end

cookbook_file '/var/lib/misc/noosfero.key' do
  notifies :run, 'execute[apt-key-add]', :immediately
end
execute 'apt-key-add' do
  command 'apt-key add /var/lib/misc/noosfero.key'
  action :nothing
end
file '/etc/apt/sources.list.d/noosfero.list' do
  content "deb http://download.noosfero.org/debian/wheezy-1.3 ./"
  notifies :run, 'execute[apt-get update]', :immediately
end
execute 'apt-get update' do
  action :nothing
end

# packages needed by common plugins
package 'ruby-rakismet'
package 'ruby-slim'
package 'ruby-net-ldap'
package 'ruby-progressbar'
package 'ruby-magic'
package 'ruby-sinatra'

Array(node['noosfero']['extra_packages']).each do |pkg|
  package pkg
end

# Only works on from chef 0.10.6 and on

package 'noosfero' do
  response_file 'noosfero.preseed.erb'
end

template '/etc/noosfero/unicorn.rb' do
  mode 0644
  notifies :restart, 'service[noosfero]', :delayed
  only_if "dpkg --compare-versions $(dpkg-query --show noosfero | awk '{print($2)}') gt 1.3~"
end

package 'noosfero-apache'
package 'varnish'
package 'libapache2-mod-rpaf'
package 'pound'

cookbook_file "/etc/apache2/ports.conf" do
  mode 0644
  notifies :restart, 'service[apache2]', :delayed
end
template "/etc/apache2/sites-available/noosfero" do
  mode 0644
  source 'sites_available_noosfero.erb'
  notifies :restart, 'service[apache2]', :delayed
end
cookbook_file "/etc/varnish/default.vcl" do
  mode 0644
  source 'varnish_default.vcl'
  notifies :restart, 'service[varnish]', :delayed
end
cookbook_file "/etc/default/varnishncsa" do
  mode 0644
  notifies :restart, 'service[varnishncsa]', :delayed
end
cookbook_file '/etc/pound/pound.cfg' do
  notifies :restart, 'service[pound]'
end
file '/etc/default/pound' do
  content "startup=1\n"
  notifies :restart, 'service[pound]'
end
cookbook_file '/etc/noosfero/noosfero.pem' do
  mode 0600
  notifies :restart, 'service[pound]'
end
service 'apache2' do
  supports reload: true
  action :nothing
end
service 'varnish' do
  action :nothing
end
service 'varnishncsa' do
  action :nothing
end
service 'noosfero' do
  action :nothing
end
service 'pound' do
  action :nothing
end

cookbook_file '/usr/local/bin/noosfero-bootstrap' do
  mode '0755'
end

execute 'noosfero-bootstrap' do
  command [
    "rails runner -e production /usr/local/bin/noosfero-bootstrap",
    'touch /var/lib/noosfero-data/bootstrap.stamp'
  ].join(' && ')
  not_if 'test -f /var/lib/noosfero-data/bootstrap.stamp'
  environment({
    'NOOSFERO_NAME' => node['noosfero']['name'],
    'NOOSFERO_EMAIL' => node['noosfero']['email'],
    'NOOSFERO_DOMAIN' => node['noosfero']['domain'],
  })
  user 'noosfero'
  group 'noosfero'
  cwd '/usr/share/noosfero'
end

plugins = node['noosfero']['plugins'] || []
unless plugins.empty?
  execute 'list-plugins' do
    command 'find /etc/noosfero/plugins | sort > /etc/noosfero/plugins.list.old'
  end

  execute 'enable-plugins' do
    command ['noosfero-plugins', 'enable', *plugins].join(' ')
  end

  execute 'list-new-plugins' do
    command 'find /etc/noosfero/plugins | sort > /etc/noosfero/plugins.list'
  end

  execute 'activate-plugins' do
    command 'service noosfero restart' # already runs migrations
    not_if 'diff /etc/noosfero/plugins.list.old /etc/noosfero/plugins.list'
  end
end
