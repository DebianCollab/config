package 'git'

execute 'git clone https://gitlab.com/DebianCollab/theme-dcc.git /usr/share/noosfero/public/designs/themes/debian' do
  not_if { File.exists?('/usr/share/noosfero/public/designs/themes/debian') }
end

link '/usr/share/noosfero/public/designs/themes/default' do
  to 'debian'
end
